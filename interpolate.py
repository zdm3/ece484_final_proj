import numpy as np
import csv
from scipy.interpolate import CubicSpline
import matplotlib.pyplot as plt
import csv

# Input data points
data = [
    [-25.014200, -21.355800, 0.00000, 0.500000, 0.00788],
    [-30.759428, -21.488939, 0.00000, 0.500000, 0.00788],
    [-35.605194, -22.749678, 0.00000, 0.500000, 0.00788],
    [-37.019352, -24.976494, 0.00000, 0.500000, 0.00788],
    [-36.254734, -28.298300, 0.00000, 0.500000, 0.00788],
    [-32.427460, -30.352312, 0.00000, 0.500000, 0.00788],
    [-28.993677, -28.961426, 0.00000, 0.500000, 0.00788],
    [-27.882257, -25.529676, 0.00000, 0.500000, 0.00788],
    [-26.874109, -22.411884, 0.00000, 0.500000, 0.00788],
    [-23.318579, -21.213005, 0.00000, 0.500000, 0.00788],
    [-5.503869, -20.926449, 0.00000, 0.500000, 0.00788],
    [4.012156, -21.356438, 0.00000, 0.500000, 0.00788]
]

# Number of rows in the output CSV
num_rows = 1000

# Linearly interpolate the data
interpolated_data = []
for i in range(5):  # Interpolate the first four columns
    interpolated_col = np.interp(
        np.linspace(0, 1, num_rows),
        np.linspace(0, 1, len(data)),
        [point[i] for point in data]
    )
    interpolated_data.append(interpolated_col)

x = np.linspace(0, 1, len(data))
cs = [CubicSpline(x, [point[i] for point in data]) for i in range(4)]
for i in range(4):
    interpolated_data[i] = cs[i](np.linspace(0, 1, num_rows))

# Transpose the data for CSV writing
interpolated_data = np.array(interpolated_data).T

# Write the data to a CSV file
with open('interpolated_data.csv', 'w', newline='') as csvfile:
    csv_writer = csv.writer(csvfile)
    for row in interpolated_data:
        csv_writer.writerow(row)


# Load the interpolated data from the CSV file
data = []
with open('interpolated_data.csv', 'r') as csvfile:
    csv_reader = csv.reader(csvfile)
    for row in csv_reader:
        data.append([float(value) for value in row])

# Split the data into columns
x_values = [row[0] for row in data]
y_values = [row[1] for row in data]
z_values = [row[2] for row in data]

# Create a 2D plot
plt.figure(figsize=(10, 6))
plt.plot(x_values, y_values, label='X vs Y', marker='o', markersize=2)
plt.title('Interpolated Data Plot')
plt.xlabel('X')
plt.ylabel('Y')

# Show the plot
plt.grid(True)
plt.legend()
plt.show()

